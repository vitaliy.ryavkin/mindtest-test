﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MindBoxTest;

namespace MindBoxTest.Tests
{
    public class CompileTimeTest
    {
        [Fact]
        public void CountSquare()
        {
            Triangle triangle = new(3, 4, 5);
            Circle circle = new(5);

            List<IFigure> figures = new();

            figures.Add(circle);
            figures.Add(triangle);

            // Коллекция figures знает лишь то, что объекты коллекции реализуют определенный интерфейс
            Tuple<Double, Double> result = new(figures[0].CountSquare(), figures[1].CountSquare());
            Tuple<Double, Double> answer = new(78.53981633974483, 6);
           
            Assert.Equal(answer, result);
        }

    }
}
