﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MindBoxTest;

namespace MindBoxTest.Tests
{
    public class TriangleTests
    {
        [Theory]
        [InlineData(-1, 2, 3)]
        [InlineData(1, -2, 3)]
        [InlineData(1, 2, -3)]
        public void NegativeSides(double firstSide, double secondSide, double thirdSide)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                Triangle triangle = new(firstSide, secondSide, thirdSide);
            });
        }

        [Fact]
        public void TriangleIsRectangular()
        {
            Triangle triangle = new(3, 4, 5);
            Assert.True(triangle.IsRectangular());
        }

        [Fact]
        public void TriangleIsNotRectangular()
        {
            Triangle triangle = new(3, 4, 6);
            Assert.False(triangle.IsRectangular());
        }

        [Fact]
        public void CalculateSquare() 
        {
            Triangle triangle = new(5, 12, 13);

            Assert.Equal(30, triangle.CountSquare());
        }
    }
}
