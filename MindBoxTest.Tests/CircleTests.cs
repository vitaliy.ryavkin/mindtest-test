using MindBoxTest;

namespace MindBoxTest.Tests
{
    public class CircleTests
    {
        [Theory]
        [InlineData(3.3)]
        [InlineData(2.3)]
        [InlineData(5)]
        [InlineData(4)]
        public void CountCircleSquare(double radius)
        {
            Circle circle = new(radius);

            Assert.Equal(radius*radius*Math.PI, circle.CountSquare());
        }

        [Fact]
        public void CreateCircleWithNegativeRadius()
        {
            double radius = -123;

            Assert.Throws<ArgumentException>(() =>
            {
                Circle circle = new(radius);
            });
        }

        [Fact]
        public void CountCircleSquareWithRadius2()
        {
            Circle circle = new(2);

            Assert.Equal(4*Math.PI, circle.CountSquare());
        }

        [Fact]
        public void CountCircleSquareWithRadius0()
        {
            Circle circle = new(0);

            Assert.Equal(0, circle.CountSquare());
        }
    }
}