USE [MindtestDB]

-- Создание таблиц

CREATE TABLE dbo.Products
(
ProductId UNIQUEIDENTIFIER NOT NULL PRIMARY KEY DEFAULT NEWID(),
Name VARCHAR (255) NOT NULL 
);

CREATE TABLE dbo.Categories (
	CategoryId UNIQUEIDENTIFIER NOT NULL PRIMARY KEY DEFAULT NEWID(),
	Name VARCHAR (255) NOT NULL
);

CREATE TABLE dbo.ProductCategories (
	ProductId UNIQUEIDENTIFIER FOREIGN KEY REFERENCES Products(ProductId),
	CategoryId UNIQUEIDENTIFIER FOREIGN KEY REFERENCES Categories(CategoryID),
	PRIMARY KEY (ProductId, CategoryId)
);

-- Заполнение таблиц необходимыми данными

INSERT INTO Products (Name)
	VALUES ('War and Peace'), ('Ibanez  FRH10N'), ('Soy Milk'), ('Renault Arkana');

INSERT INTO Categories(Name)
	VALUES ('Books'), ('Musical Instruments'), ('Guitars'), ('Food');

INSERT INTO ProductCategories (ProductId, CategoryId)
	VALUES ('7F928E87-887F-4365-AFFB-E9A75F056BDD', '3C4EFBA1-9B7F-48AA-A488-F5ACC956AB6F'),
			('7F928E87-887F-4365-AFFB-E9A75F056BDD','6B9347A7-E445-4102-96F8-C69C27B62C83'),
			('03B23B34-7C00-4C64-A580-C5D5DD16AB9C', '5E76B365-F98C-408D-94F3-D6E49FB087C1'),
			('1CC0F672-AC54-4452-B3E8-F86DB2BED98F', 'CA0771A9-D64E-4BB8-9DAF-772261971464');

-- Запрос для выбора всех пар «Имя продукта – Имя категории». Если у продукта нет категорий, то его имя все равно выводится.

SELECT Products.Name as "Имя продукта", Categories.Name as "Имя категории"
FROM Products
	LEFT JOIN ProductCategories 
		ON Products.ProductId = ProductCategories.ProductId
	LEFT JOIN Categories
		ON ProductCategories.CategoryId = Categories.CategoryId;