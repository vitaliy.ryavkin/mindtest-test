﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MindBoxTest
{
    public class Triangle : IFigure
    {
        private double _firstSide;
        private double _secondSide;
        private double _thirdSide;

        public double FirstSide 
        {
            get { return _firstSide; }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Triangle's side must be greater than zero!");
                }

                _firstSide = value;
            }
        }

        public double SecondSide
        {
            get { return _secondSide; }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Triangle's side must be greater than zero!");
                }

                _secondSide = value;
            }
        }

        public double ThirdSide
        {
            get { return _thirdSide; }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Side of the triangle must be greater than zero!");
                }

                _thirdSide = value;
            }
        }

        public Triangle(double firstSide, double secondSide, double thirdSide)
        {
            FirstSide = firstSide;
            SecondSide = secondSide;
            ThirdSide = thirdSide;

            if (FirstSide + SecondSide <= ThirdSide || FirstSide + ThirdSide <= SecondSide || SecondSide + ThirdSide <= FirstSide)
                throw new Exception("A triangle with specified sides cannot exist");
        }

        public double CountSquare()
        {
            double semiperimeter = (FirstSide + SecondSide + ThirdSide) / 2;

            return Math.Sqrt(semiperimeter * (semiperimeter - FirstSide) * (semiperimeter - SecondSide) * (semiperimeter - ThirdSide));
        }

        public bool IsRectangular()
        {
            double[] sides = { FirstSide, SecondSide, ThirdSide };

            Array.Sort(sides);

            return sides[2] * sides[2] == sides[0] * sides[0] + sides[1] * sides[1];
        }
    }
}
