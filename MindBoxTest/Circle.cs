﻿namespace MindBoxTest
{
    public class Circle : IFigure
    {
        private double _radius;

        public double Radius
        {
            get { return _radius; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Radius of the circle cannot be less than zero");
                }

                _radius = value;
            }
        }

        public Circle(double radius)
        {
            Radius = radius;
        }

        public double CountSquare()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }
    }
}